#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#tells the operating system to use the envcommand to find python3 to execute the rest of the program

"""
fasta sequence verification
"""

__author__ = 'Samer El Khoury'

import argparse
import sys
from adn import is_valid

def create_parser():
    """ Declares new parser and adds parser arguments """
    program_descrtiption = ''' reading fasta file and checking sequence format '''
    parser = argparse.ArgumentParser(add_help=True, description=program_descrtiption)
    parser.add_argument('-i', '--inputfile', default=sys.stdin,
                        type=argparse.FileType("r"), required=True)
    return parser

def main():
    """checks if the sequence is valide + prints the sequence + length"""
    parser = create_parser()
    args = parser.parse_args()
    args = args.__dict__
    print(args["inputfile"])
    fasta = args["inputfile"].readlines()
    file = []
    for element in fasta:
        if not element.startswith('>'):
            element = element.replace("\n", "")
            file.append(element)
    for element in file:
        if element == "":
            continue
        if not is_valid(element):
            print("la sequence n'est pas valide "+ element)
        else:
            print("la sequence est valide " + element +" "+ "len=" +str(len(element)))


if __name__ == "__main__":
    main()
