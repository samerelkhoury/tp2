# TP2 : Argparse

input : fasta file 

## ABOUT :
This program reads the fasta file,and checks if the ADN sequences are valid
(valid = sequence with only A,T,C,G)
if the sequence is valide a message will pop up with the sequence and the sequence's length
if not another message that the sequence isn't valid will pop up

## COMMANDS TO RUN THE PROGRAM :
python3 select_fasta.py -i [fasta file]

## HELP :
python3 select_fasta.py -h

## Authors :
SAMER EL KHOURY
